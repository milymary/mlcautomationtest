package com.mlc.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MLCReqADemoPage extends PageObject{

	
	@FindBy(xpath="//label[contains(text(),'Name')]//following::input[1]")
	WebElementFacade nametxtField;
	
	@FindBy(xpath="//label[contains(text(),'Email')]//following::input[1]")
	WebElementFacade emailIDtxtField;
	
	
	@FindBy(xpath="//label[contains(text(),'Phone')]//following::input[1]")
	WebElementFacade phnNumtxtField;
	
	
	//fill up the name field in the form
	public void fillUpName(String userName) {
		nametxtField.sendKeys(userName);
	}

	//fill up the email id field in the form
	public void emailID(String userEmail) {
		emailIDtxtField.sendKeys(userEmail);
	}
	
	//fill up the phone number field in the form
	public void phnnuM(String userPhn) {
		phnNumtxtField.sendKeys(userPhn);
	}
	


}
