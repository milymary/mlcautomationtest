package com.mlc.pages;


import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class SimpleTaxcalcPage extends PageObject {


	@FindBy(xpath="//select[@name='ddl-financialYear']")
	WebElementFacade incomeYrdrpdown;
	
	@FindBy(xpath="//select[@name='ddl-residentPartYearMonths']")
	WebElementFacade monthsOfResd;
	
	@FindBy(xpath="//input[@name='texttaxIncomeAmt']")
	WebElementFacade taxableincometxt;
	
	@FindBy(xpath="//button[contains(text(),'Submit')]")
	WebElementFacade submitBtn;
	
	//this method selects the financial year from drop down
	public void selectFinyear(String incYr) {
	
	incomeYrdrpdown.selectByVisibleText(incYr);
    
	}
	
	//this method enters the income 
	public void entertaxableIncome(String taxInc) {
		taxableincometxt.sendKeys(taxInc.trim());
	
		
	}
	
	//this method selects the residency status
	public void selectResidencyStat(String resStat) {
		String residencyInputParam=resStat.trim();
		
		WebElement residencyStatradioBtn = getDriver().findElement(By.xpath("//span[contains(text(),'"+residencyInputParam+"')]"));
		residencyStatradioBtn.click();
		
	}

	//select montsh of residency if condition applies 
	public void selectMonthsOfResidency(String mnthsOfres) {
		
		monthsOfResd.selectByVisibleText(mnthsOfres.trim());
	}
	
	//method to submit the tax calculator form
	public void submitForm() {
		submitBtn.click();
	}
}
