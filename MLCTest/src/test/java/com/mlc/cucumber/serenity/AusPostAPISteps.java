package com.mlc.cucumber.serenity;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;

import com.mlc.utils.ReusableSpecifications;

import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AusPostAPISteps {

	
		@Step
		public void requestValidationForAusPostAPI(String expectedCost, String wghtInKG, String countryCode) {
			RestAssured.baseURI="https://digitalapi.auspost.com.au";
			
			System.out.println("argexpectedCost" +  expectedCost );
			System.out.println("wghtInKG" + wghtInKG );

			System.out.println("countryCode" + countryCode );
			
			String CC=countryCode;
			String wght=wghtInKG;
			String cost=expectedCost;
			
			SerenityRest.given().headers("AUTH-KEY","5bb3b769-c32c-4085-bd94-e9106cd9d1e8")
								.param("country_code", CC)
								.param("weight", wght)
								.param("service_code", "INT_PARCEL_STD_OWN_PACKAGING")
						.when()
						.get("/postage/parcel/international/calculate.json")
						.then()
						.log()
						.all()
						.assertThat()
						.statusCode(200).spec(ReusableSpecifications.getGenericResponseSpec(expectedCost));
								
			
			
			
	}
	
	
}

